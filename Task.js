
import React from 'react'
import { useState } from 'react'
export const Task = () => {
    const [num, setNum] = useState('')
    const [big, setBig] = useState('')

    const res = () => {
      const arr = num.split(' ');

      const obj = {};

      for (let i = 0; i < arr.length; i++) {
          if (!obj.hasOwnProperty(arr[i])) obj[arr[i]] = 1;
          else obj[arr[i]] = obj[arr[i]] + 1;
      }

      setBig(Object.entries(obj).map(el => <li key={el}>{el[0]}: {el[1]}</li>))
  }
  return (
    <div>
        <input onChange={e=>setNum(e.target.value)} value={num} placeholder='Enter num'/>
        <button onClick={res}>Biggest</button>
        <ul>{big}</ul>
    </div>
  )
}